# Claw Machine (Crane Machine) Game

## Description

A claw crane, toy crane or skill crane is a game where we can get rewards or prizes by catching the ball with the crane machine.

## How to run the game

1. clone the app to your local

2. npm install

3. npm start

## How to play
1. You can use arrow left and right to move the crane machine and arrow down to catch the ball.

2. Or you can use mouse simply by clicking once will move the crance machine on your clicked position horizontally of course. Clicking twice to lower the crane machine.

3. In mobile, same with mouse, tap once for your preferred location. Click twice to lower the crane/

4. After lowering the crane, if you succeed to catch the ball, there will be a congratulatory sound and a big text showing your rewards in the center.

5. If you fail to catch them, you can try again simply by inputting your email on the prompt.