// Import Application class that is the main part of our PIXI project
import {
	Application
} from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import {
	Renderer
} from '@pixi/core' // Renderer is the class that is going to register plugins

import {
	BatchRenderer
} from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import {
	AppLoaderPlugin
} from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import {
	Sprite
} from '@pixi/sprite'

import {
	Container
} from '@pixi/display'

import {
	Graphics
} from '@pixi/graphics';
Renderer.registerPlugin('graphics', Graphics);

import * as Keyboard from 'pixi.js-keyboard';
import sound from 'pixi-sound';
import * as text from '@pixi/text';

import firebase from 'firebase/app';
import 'firebase/firestore';
// Firebase init
firebase.initializeApp({
	apiKey: "AIzaSyAhskAt0nPkPlNQpdlJgtzzUrCS0v5W64w",
	authDomain: "buyble-mvp.firebaseapp.com",
	databaseURL: "https://buyble-mvp.firebaseio.com",
	projectId: "buyble-mvp",
	storageBucket: "buyble-mvp.appspot.com",
	messagingSenderId: "129776807064",
	appId: "1:129776807064:web:d748c8fd2cc55de0b774ed"
});

var db = firebase.firestore();

// App with width and height of the page
const app = new Application({
	width: window.innerWidth,
	height: window.innerHeight,
	resizeTo: window
})
// Create Canvas tag in the body
document.body.appendChild(app.view);
//add background color on canvas
app.renderer.backgroundColor = 0x061639;

//declare global var
var balls = [],
	claw, ball, icons, state, isGoingDown = false,
	cable, icon, click = 0,
	randomX, isCaught = false,
	ballCaughtIndex,
	doneLoop = false,
	isGoingUp = false,
	cables = [],
	texts = [],
	textDisplayed, gifts = [],
	isTextDisplayed = false,
	iconIndex,
	soundPlayed = true,
	targetLocation, ableToMove = true,
	locationClicked, clawX;

app.loader
	.add('ball', "./assets/sphere.png")
	.add('claw', "./assets/claw.png")
	.add('cable', "./assets/cable.png")
	.add('freeDelivery', "./assets/freeDeliveryIcon.png")
	.add('sale', "./assets/saleIcon.png")
	.add('shoppingBag', "./assets/shoppingBagIcon.png")
	.add('shoppingCart', "./assets/shoppingCartIcon.png")
app.loader.load(() => {
	//icon names
	icons = ['freeDelivery', 'sale', 'shoppingBag', 'shoppingCart'];

	//gifts inside the icon
	gifts = ['Free Delivery coupon', 'Discount 15% of your cart goods', 'Try again next time!', 'Sorry you almost got it!']
	
	//show plane
	claw = new Sprite.from('claw');

	//initializing sounds into the respective names
	sound.add({
		ballCaughtSound: './assets/ballCaught.mp3',
		goingDownSound: './assets/goingDown.mp3',
	});

	createBalls()
	responsive()
	showClaw()
})

function isBallCaught(r1, r2) {
	//Define the variables we'll need to calculate
	let hit;

	//hit will determine whether there's a collision
	hit = false;

	if (r1 != null && r2 != null) {
		if (r1.x - r1.width / 2 <= r2.x - r2.width / 2 && r1.x + r1.width / 2 >= r2.x + r2.width / 2) {
			hit = true;
		} else {

			//There's no collision on the x axis
			hit = false;
		}

		//`hit` will be either `true` or `false`
		return hit;
	}
};

//when screen is clicked
app.renderer.plugins.interaction.on('pointerup', moveClaw);

function moveClaw(event) {
	click++;
	if (ableToMove) {
		//condition where it's clicked twice or more on the same spot
		if (click > 1 && (event.data.global.x == locationClicked || (event.data.global.x - locationClicked <= 5 && event.data.global.x - locationClicked > 0) || (locationClicked - event.data.global.x <= 5 && locationClicked - event.data.global.x > 0))) {
			claw.vy = 5;
			claw.vx = 0;
			isGoingDown = true;
		} else if (!isGoingDown && !isGoingUp) {
			clawX = Math.round(claw.x);
			targetLocation = Math.round(event.data.global.x);
			if (clawX > targetLocation) {
				if (clawX - targetLocation < 5) {
					claw.vx = -(clawX - targetLocation);
				} else {
					claw.vx = -5;
				}
			} else if (clawX < targetLocation) {
				if (targetLocation - clawX < 5) {
					claw.vx = targetLocation - clawX;
				} else {
					claw.vx = 5;
				}
			}
		}
	}

	//saving the location clicked to verify if it's clicked on the same spot
	locationClicked = event.data.global.x;

	//change click count to 0 when a second has passed
	if (click == 1) {
		setTimeout(function () {
			click = 0
		}, 1000);
	}
}

function createBalls() {
	for (var i = 0; i <= 15; i++) {
		//create the ball
		ball = new Sprite.from('ball');
		ball.anchor.set(0.5);
		//assign the icons inside the ball randomly
		let randomIcon = get_random(icons);
		texts.push(randomIcon)
		icon = new Sprite.from(randomIcon);
		//locate the icon position inside the ball
		icon.x = ball.x - icon.width + 40;
		icon.y = ball.y - icon.height + 40;
		//locate the ball on the screen randomly
		randomX = Math.floor(Math.random() * app.screen.width);
		ball.x = randomX;
		ball.y = app.screen.height - (ball.height / 2);
		//add speed for the movement of the ball
		ball.speed = Math.random() * (5 - 1) + 1;
		ball.addChild(icon);
		balls.push(ball);
		app.stage.addChild(ball);
	}
}

//get random number
function get_random(list) {
	if (list.length > 0) {
		return list[Math.floor((Math.random() * list.length))];
	}
}

function showClaw() {
	// set the anchor point so the texture is centerd on the sprite
	claw.anchor.set(0.5);

	// finally lets set the claw to be at a random position..
	claw.x = claw.width / 2;
	claw.y = claw.height / 2;

	claw.vx = 0;
	claw.vy = 0;

	app.stage.addChild(claw);

	//arrows keyboard movement for the claw
	let left = keyboardArrows("ArrowLeft"),
		right = keyboardArrows("ArrowRight"),
		down = keyboardArrows("ArrowDown");

	//Left arrow key `press` method
	left.press = () => {
		if (ableToMove) {
			//Change the claw's velocity when the key is pressed
			claw.vx = -5;
			if (!isGoingDown && !isGoingUp) {
				claw.vy = 0;
			}
		}
	};

	//Left arrow key `release` method
	left.release = () => {
		//If the left arrow has been released, and the right arrow isn't down,
		//and the claw isn't moving vertically:
		//Stop the claw
		if (!right.isDown) {
			claw.vx = 0;
		}
	};

	//Right
	right.press = () => {
		if (ableToMove) {
			claw.vx = 5;
			if (!isGoingDown && !isGoingUp) {
				claw.vy = 0;
			}
		}
	};
	right.release = () => {
		if (!left.isDown) {
			claw.vx = 0;
		}
	};

	//Down
	down.press = () => {
		if (ableToMove) {
			claw.vy = 5;
			claw.vx = 0;
			isGoingDown = true;
		}
	};

	//Set the game state
	state = play;

	//Start the game loop 
	app.ticker.add(delta => gameLoop(delta));
}

function keyboardArrows(value) {
	let key = {};
	key.value = value;
	key.isDown = false;
	key.isUp = true;
	key.press = undefined;
	key.release = undefined;
	//The `downHandler`
	key.downHandler = event => {
		if (event.key === key.value) {
			if (key.isUp && key.press) key.press();
			key.isDown = true;
			key.isUp = false;
			event.preventDefault();
		}
	};

	//The `upHandler`
	key.upHandler = event => {
		if (event.key === key.value) {
			if (key.isDown && key.release) key.release();
			key.isDown = false;
			key.isUp = true;
			event.preventDefault();
		}
	};

	//Attach event listeners
	const downListener = key.downHandler.bind(key);
	const upListener = key.upHandler.bind(key);

	window.addEventListener(
		"keydown", downListener, false
	);
	window.addEventListener(
		"keyup", upListener, false
	);

	// Detach event listeners
	key.unsubscribe = () => {
		window.removeEventListener("keydown", downListener);
		window.removeEventListener("keyup", upListener);
	};

	return key;
}

function gameLoop(delta) {
	//Update the current game state:
	state(delta);
}

function play(delta) {
	//when the claw is lowered
	if (claw.y + (claw.height / 2) >= app.screen.height - (balls[0].height / 2) && !doneLoop) {
		for (var j = 0; j <= 25; j++) {
			//check if the ball is caught by the claw
			if (isBallCaught(claw, balls[j])) {
				ableToMove = false;
				ballCaughtIndex = j;
				isCaught = true;
			}
			
			//when all balls is already checked
			if (j == 25) {
				doneLoop = true;
			}
		}
	}
	//when all balls is checked
	if (doneLoop) {
		//move the claw upwards and remove the cables
		for (var i = cables.length - 1; i >= 0; i--) {
			cables[i].y -= 5;
		}
		isGoingDown = false;
		isGoingUp = true;
		//when a ball is caught
		if (isCaught && (claw.y >= claw.height / 2)) {
			//raising the claw
			claw.vy = 5;
			claw.y -= claw.vy;
			//raising the ball that was caught and the icon inside it
			balls[ballCaughtIndex].vy = 5;
			balls[ballCaughtIndex].y -= balls[ballCaughtIndex].vy;
			iconIndex = icons.indexOf(texts[ballCaughtIndex]);
			//play the sound
			sound.play('ballCaughtSound');
			//display winning text
			if (!isTextDisplayed) {
				showText();
				isTextDisplayed = true;
			}
		} else if (claw.y >= claw.height / 2) {
			//when the ball is not caught, raise only the claw
			claw.vy = 5;
			claw.y -= claw.vy;
		} else {
			//when the ball is caught by the claw, it (ball) stops moving
			if (ballCaughtIndex) {
				balls[ballCaughtIndex].vy = 0;
			} else {
				//if fails to catch, enter email to try again
				var email = prompt("Try again by entering your email");
				var re = /^(([^<>()&$#!^*`~?\[\]\\\/{}|.,%;:\s=@"']+(\.[^<>()&$#!^*`~?+\[\]\\\/{}|.,%;:=\s@"']+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				var validate = re.test(email);
				if (email == null || email == "") {
					//if it isn't entered then the claw is disabled
					ableToMove = false;
				} else if (validate) {
					//email entered here
					db.collection("claw-machine-result").add({
						email: email,
					}).then(function(docRef) {
						console.log("Document written with ID: ", docRef.id);
					})
					.catch(function(error) {
						console.error("Error adding document: ", error);
					});
				} else {
					ableToMove = false;
					alert("Email is invalid");
				}
			}
			//when the claw is at it's prior location, erase the y velocity
			claw.vy = 0;
			//reassign that the claw not moving
			isGoingUp = false;
			//reassign that all balls are not checked
			doneLoop = false;
			//reassign so the sound is not played repeatedly
			soundPlayed = true;
		}
	}
	if (!(claw.y + (claw.height / 2) >= app.screen.height - (balls[0].height / 2)) && !isGoingUp) {
		//moving the claw to the horizontal point we want
		if (!isGoingDown && !isGoingUp) {
			//when the claw position is on the target location or nearing
			if (Math.round(claw.x) == targetLocation) {
				claw.vx = 0;
			} else if (claw.x > targetLocation && claw.x - targetLocation < 5) {
				claw.vx = -(claw.x - targetLocation);
			} else if (claw.x < targetLocation && targetLocation - claw.x < 5) {
				claw.vx = targetLocation - claw.x;
			}

			claw.x += claw.vx;
		}
		claw.y += claw.vy;

		if (claw.vy > 0) {
			//if the velocity for the y axis is not zero then sound is played
			if (soundPlayed) {
				sound.play('goingDownSound');
				soundPlayed = !soundPlayed;
			}
			//show cable so the claw doesn't look like floating
			cable = new Sprite.from('cable')
			if (app.screen.width < 768) {
				cable.scale.x = 0.5;
				cable.x = claw.x - 1;
			} else {
				cable.x = claw.x - 3;
			}
			cable.y = claw.y - (claw.height / 2) - 5;
			app.stage.addChild(cable);
			cables.push(cable);
		}
	}

	//the ball movement
	for (var i = 0; i <= balls.length - 1; i++) {
		if (i != ballCaughtIndex) {
			balls[i].x -= balls[i].speed;
		}
		if (balls[i].x < -balls[i].width) {
			balls[i].x = app.screen.width + balls[i].width;
		}
	}
}

function showText() {
	//text style
	var style = new text.TextStyle({
		fontFamily: "Roboto",
		fontSize: 30,
		fill: "#fff",
		stroke: '#000000',
		strokeThickness: 4,
	});
	if (iconIndex > 1) {
		//when it's void
		textDisplayed = new text.Text(gifts[iconIndex], style);
	} else {
		// when it's not void
		textDisplayed = new text.Text('Congratulations you got ' + gifts[iconIndex], style);
	}

	//when the screen is below 768, resolution all changes to mobile
	if (app.screen.width < 768) {
		textDisplayed.style = {
			fontSize: 14
		};
	}
	textDisplayed.anchor.set(0.5);
	textDisplayed.x = app.renderer.screen.width / 2;
	textDisplayed.y = app.renderer.screen.height / 2;

	app.stage.addChild(textDisplayed);
}

function responsive() {
	var w = app.renderer.screen.width;

	if (w < 768) {
		//when the screen is below 768, resolution all changes to mobile
		//claw size change
		claw.scale.x = 0.5;
		claw.scale.y = 0.5;
		//claw vertical position
		claw.y = claw.height / 2;
		//balls size and position changed
		for (var i = 0; i <= balls.length - 1; i++) {
			balls[i].scale.x = 0.5;
			balls[i].scale.y = 0.5;
			if (balls[i].x + (balls[i].width / 2) > w) {
				balls[i].x = w - balls[i].width / 2;
			}
			balls[i].y = app.screen.height - (balls[i].height / 2);
			balls[i].children[0].x = -balls[i].children[0].width + 30;
			balls[i].children[0].y = -25;
		}
		//cable size changed
		for (var j = 0; j <= cables.length - 1; j++) {
			cables[j].scale.x = 0.5;
		}
		//winning text is changed
		if (isTextDisplayed) {
			textDisplayed.style = {
				fontSize: 14
			};
		}
	} else if (w >= 768) {
		//Screen changed to desktop
		//claw size change
		claw.scale.x = 1;
		claw.scale.y = 1;
		//claw vertical position
		claw.y = claw.height / 2;
		//balls size and position changed
		for (var i = 0; i <= balls.length - 1; i++) {
			balls[i].scale.x = 1;
			balls[i].scale.y = 1;
			if (balls[i].x + (balls[i].width / 2) > w) {
				balls[i].x = w - balls[i].width / 2;
			}
			balls[i].y = app.screen.height - (balls[i].height / 2);
			balls[i].children[0].x = -balls[i].children[0].width + 40;
			balls[i].children[0].y = -balls[i].children[0].height + 40;
		}
		//cable size changed
		for (var j = 0; j <= cables.length - 1; j++) {
			cables[j].scale.x = 0.5;
		}
		//winning text is changed
		if (isTextDisplayed) {
			textDisplayed.style = {
				fontSize: 30
			};
		}
	}
}

function resize() {
	//resize canvas
	app.renderer.view.style.width = window.innerWidth + 'px';
	app.renderer.view.style.height = window.innerHeight + 'px';
	//call responsive function
	responsive()
}

//call function when screen is resized
window.onresize = resize;